<?php	
include('config.php');

function authenticate($username, $password) {
	$query = "SELECT count(*) FROM Client
			  WHERE id='{$username}' AND password='{$password}'";
	$result = pg_query(DB, $query);
	$count = pg_fetch_assoc($result)['count'];
	if($count == 1) return true;
	else return false;
}


/* assign logic
**/








/* end assign logic
**/

function getClientByName($username) {
	$query = "SELECT * FROM Client
         	  WHERE id='{$username}'";
	$result = pg_query(DB, $query);
	return pg_fetch_assoc($result);
}

function checkClassExistByID($class_id) {
	$query = "SELECT count(*) FROM class
			  WHERE id='{$class_id}'";
	$result = pg_query(DB, $query);
	return pg_fetch_assoc($result);
}

function addAssignTable($client, $class) {
	$query = "INSERT INTO Assign(client, class)
			  VALUES('{$client}', '{$class}')";
	pg_query(DB, $query);
}

function deleteAssignTable($client, $class) {
	$query = "DELETE FROM Assign
			  WHERE client = '{$client}' AND class = '{$class}'";
	pg_query(DB, $query);
}

function getAssignTable($username) {
	$query = "SELECT class FROM assign 
			  WHERE client = '{$username}'";
	return pg_query(DB, $query);
}

function getClassTable() {
	$query = "SELECT * FROM Class";
	$result = pg_query(DB, $query);
	return $result;
}

function getClassByID($class_id) {
	$query = "SELECT * FROM Class
			  WHERE id = '{$class_id}';";
	$result = pg_query(DB, $query);
	return pg_fetch_assoc($result);
}

function getClassByName($class_name) {
	$query = "SELECT * FROM Class
			  WHERE name like '%{$class_name}%';";
	$result = pg_query(DB, $query);
	return pg_fetch_assoc($result);
}

function getClassBySub($subject_id) {
	$query = "SELECT * FROM Class
			  WHERE subject = '{$subject_id}';";
	$result = pg_query(DB, $query);
	return pg_fetch_assoc($result);
}

function getSumCredit($student_id) {
	$query = "SELECT SUM(s.credit) FROM Assign a, Class c, Subject s
			  WHERE a.client = '{$student_id}'
			  AND a.class = c.id
			  AND c.subject = s.id";
	$result = pg_query(DB, $query);
	return pg_fetch_assoc($result);
}

function getSubject($subject_id) {
	$query = "SELECT * FROM Subject
			  WHERE id = '{$subject_id}';";
	$result = pg_query(DB, $query);
	return pg_fetch_assoc($result);
}

function getRequireTable($subject_id) {
	$query = "SELECT require_subject FROM Require
			  WHERE subject = '{$subject_id}';";
	return pg_query(DB, $query);		  
}

function getResult($subject_id) {
	$query = "SELECT grade from Result
			  WHERE Client = '{$_SESSION['id']}'
			  AND Subject = '{$subject_id}'";
	$result = pg_query(DB, $query);
	return pg_fetch_assoc($result);		  
}

?>
<?php
ob_start();
session_start();
include('a.php');
$privilege = getClientByName($_SESSION['client_id'])['privilege'];
if($privilege == '1') {
	header('location: student.php');
} else if($privilege == '3') {
	header('location: admin.php');
}

if(isset($_GET['login'])) {
	$authenticate = authenticate($_GET['client_id'], $_GET['password']);
	if($authenticate) {
		$_SESSION['client_id'] = $_GET['client_id'];
		$client = getClientByName($_SESSION['client_id']);
		$privilege = $client['privilege'];
		if($privilege == '1') {
			header('location: student.php');
		} else {
			header('location: admin.php');
		}
	}
	else {
		echo "<div class=\"warning\"><h3>Wrong username or password</h3></div>";
	}
}
ob_end_flush();
?>
<html>
<head>
	<title>DK-SIS</title>
	<link rel="stylesheet" href="index.css">
</head>
<body>
<div class="title">
	<h1>TRANG DANG KY TIN CHI</h1>
  	<h1>TRUONG DAI HOC BACH KHOA HA NOI</h1>
</div>
<div class="form">
	<form action="index.php" method="get">
		<input type="text" name="client_id" placeholder="username" required><br>
		<input type="password" name="password" placeholder="password" required><br>
		<input type="submit" name="login" value="login">
	</form>
</div>
</body>
</html>

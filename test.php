<?php
session_start();
include('config.php');
include_once('a.php');

// return false if exist, true otherwise
function checkClassInClassTable($class_id) {
	$query = "SELECT Count(*) FROM Class
	WHERE id='{$class_id}'";
	$result = pg_query(DB, $query);
	$count = pg_fetch_assoc($result)['count'];
	if($count == '1') return false;
	else return true;
}

// return true if exist, false otherwise
function checkClassInAssignTable($class_id) {
	$assign = getAssignTable($_SESSION['client_id']);
	while($result = pg_fetch_assoc($assign)) {
		if($result['class'] == $class_id) {
			return true;
		}
	}
	return false;
}

// return true if excess, false otherwise
function checkExcessCredit($class_id) {
	$sum = getSumCredit($_SESSION["client_id"])["sum"];
	$class = getInfoClassByID($class_id);
	$subject = getInfoSubject($class["subject"]);
	if($subject["credit"] + $sum > 1) {
		return true;
	} else {
		return false;
	}
}

// return true if not pass, false otherwise

// return true if conflict, false otherwise

function checkAssign($class_id) {
	if(!checkClassInClassTable($class_id)) {		// false thi tim tiep
		if(!checkClassInAssignTable($class_id)) {   // false thi tim tiep
			if(!checkExcessCredit($class_id)) {   	// false thi tim tiep
				if(/*check require table*/) {		// false thi tim tiep
					if(/*check time table*/) {		// false thi tim tiep

					} else {
						return 5;
					}
				} else {
					return 4;
				}
			} else {
				return 3;
			}
		} else {
			return 2;
		}
	} else {
		return 1;
	} 
}

$error = checkAssign('93355');
if($error) echo 1;
else echo 2;


//Check if student didn't pass requirement subject
$require = getRequireTable($_GET["class_id"]);
while($result = pg_fetch_assoc($require)) {
	$require_subject = $result['require_subject'];
	$grade = getResult($require_subject)['grade'];
	if($grade === 'F') {
		$_SESSION['warning'] = "Not pass requirements";
		header('location: student.php');
	}
}

//Check if class's time conflict with others in table
while($result = pg_fetch_assoc($assign)) {
	$assign_class = getInfoClassByID($result['class']);
	if($class['week_day'] === $assign_class['week_day']) {
		if(!($class['time_start'] > $assign_class['time_end']
			||  $class['time_end'] < $assign_class['time_start'])) {
				$_SESSION['warning'] = "Time conflict";
		}
	}

?>
<html>
<head>
	<link rel="stylesheet" href="list.css">
	<script src="jquery.min.js"></script>
	<style>
		input {
        		resize: horizontal;
        		width: 150px;
    		}

    	input:active {
        		width: auto;   
    	}

    	input:focus {
        		min-width: 200px;
    		}
	</style>
</head>
<body>
	<input type="text" id="class_id" class="search-key" placeholder="ClassID">
	<input type="text" id="class_name" class="search-key" placeholder="ClassName">
	<input type="text" id="subject_id" class="search-key" placeholder="SubjectID">
	<input type="text" id="place" class="search-key" placeholder="Place">
	<input type="text" id="week_day" class="search-key" placeholder="WeekDay">
	<input type="text" id="time_start" class="search-key" placeholder="TimeStart">
	<input type="text" id="time_end" class="search-key" placeholder="TimeEnd">
	<table border="1" cellspacing="0" id="class_info">
		<tr>
			<th>Class</th>
			<th>Name</th>
			<th>Subject</th>
			<th>Place</th>
			<th>WeekDay</th>
			<th>TimeStart</th>
			<th>TimeEnd</th>
		</tr>
		<?php
		include('config.php');
		//tinh total records
		$result = pg_query(DB, 'select count(*) as total from class');
		$row = pg_fetch_assoc($result);
		$total_record = $row['total'];
		//tinh current page va limit
		$current_page = (isset($_GET['page'])) ? $_GET['page'] : 1;
		$limit = 2;
		//tinh total page
		$total_page = ceil($total_record/$limit);
		//gioi han current page
		if($current_page > $total_page) $current_page = $total_page;
		else if($current_page < 1) $current_page = 1;
		//tim start
		$start = ($current_page - 1)*$limit;
		//lay danh sach voi start va limit
		$query = "SELECT * FROM Class ORDER BY subject ASC LIMIT $limit OFFSET $start";
		$result = pg_query(DB, $query);
		//hien thi danh sach
		while($class = pg_fetch_assoc($result)) {
			echo '<tr>';
			echo "<td data-input=\"class_id\"> ". $class['id'] ."</td>";
			echo "<td data-input=\"class_name\"> ". $class['name'] ."</td>";
			echo "<td data-input=\"subject_id\"> ". $class['subject'] ."</td>";
			echo "<td data-input=\"place\"> ". $class['place'] ."</td>";
			echo "<td data-input=\"week_day\"> ". $class['week_day'] ."</td>";
			echo "<td data-input=\"time_start\"> ". $class['time_start'] ."</td>";
			echo "<td data-input=\"time_end\"> ". $class['time_end'] ."</td>";
			echo "</tr>";
		}
		?>
	</table><br>
	<div class="page_number">
		<?php
		//hien thi day so trang, cac nut next, prev
		if($current_page > 1 && $total_page > 1)
			echo '<a href="list.php?page='.($current_page-1).'">Prev</a> | ';
		for($i = 1; $i <= $total_page; $i++) {		// update: hien so ... 
			if($i == $current_page) {
				echo '<span>'.$i.'</span> | ';
			} else {
				echo '<a href="list.php?page='.$i.'">'.$i.'</a> | ';
			}
		}
		if ($current_page < $total_page&& $total_page > 1)
        	echo '<a href="list.php?page='.($current_page+1).'">Next</a> | ';
	?>
	</div>
	<script>
		var $filterableRows = $("#class_info").find('tr').not(':first');
		var $inputs = $('.search-key');

		$inputs.on('input', function() {
			$filterableRows.hide().filter(function() {
  			return $(this).find('td').filter(function() {
    	
      			var tdText = $(this).text().toLowerCase();
      			var	inputValue = $('#' + $(this).data('input')).val().toLowerCase();
    
    			return tdText.indexOf(inputValue) != -1;
    
    			}).length == $(this).find('td').length;
  			}).show();
		});
	</script>
	<div class="page">
		<input type="button" onclick="location.href='student.php';" value="Assign">
		<input type="button" onclick="location.href='logout.php';" value="Log out">
	</div>
</body>
</html>
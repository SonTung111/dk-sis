<?php
session_start();
include_once('a.php');
//include('assign.php');
if(isset($_GET['assign'])) {
	$error = checkAssign($_GET['class_id']);
	if($error) addAssignTable($_SESSION['id'], $class['id']);
	else echo "<div class=\"warning\"></div>";
}

if(isset($_GET['delete'])) {
	deleteAssingTable($_SESSION['client_id'], $_GET['class_id']);
}
?>
<html>
<head>
	<link rel="stylesheet" href="student.css">
</head>
<body>
	<table border="1" cellspacing="0">
		<tr>
			<th>Class</th>
			<th>Name</th>
			<th>Subject</th>
			<th>Place</th>
			<th>WeekDay</th>
			<th>TimeStart</th>
			<th>TimeEnd</th>
		</tr>
		<?php
			$result = getAssignTable($_SESSION['client_id']);
			while($row = pg_fetch_assoc($result)) {
				$class = getClassByID($row['class']);
				echo '<tr>';
				echo "<td> ". $class['id'] ."</td>";
				echo "<td> ". $class['name'] ."</td>";
				echo "<td> ". $class['subject'] ."</td>";
				echo "<td> ". $class['place'] ."</td>";
				echo "<td> ". $class['week_day'] ."</td>";
				echo "<td> ". $class['time_start'] ."</td>";
				echo "<td> ". $class['time_end'] ."</td>";
				echo "</tr>";
			}	
		?>
	</table><br>
	<div class="form">
		<form method="get" action="student.php">
			<input type="text" name="class_id" placeholder="classId">
			<input type="submit" name="assign" value="assign">
		</form>
		<form method="get" action="student.php">
			<input type="text" name="class_id" placeholder="classId">
			<input type="submit" name="delete" value="delete">
		</form>
	</div>
	<div class="page">
		<input type="button" onclick="location.href='list.php';" value="List">
		<input type="button" onclick="location.href='logout.php';" value="Log out">
	</div>
</body>
</html>
